#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  readGenoList.py
#  
#  Copyright 2019 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import sources.GraphConv as GRN
from sklearn.metrics import matthews_corrcoef, accuracy_score, classification_report, f1_score
from sources.readPPI import readPPIsSparseTensor
import csv, os, sys, pickle
import numpy as np
import torch as t
import sources.utils as U
import parseAnnovarMultianno as PAM
import socket
from sklearn.preprocessing import StandardScaler, RobustScaler, MinMaxScaler, MaxAbsScaler
from sklearn.model_selection import KFold, GroupKFold
from sources.ATsamples import ATsamples, IGNORE_INDEX
from sources.readIonome import parseIonomeComplete
from sklearn.neighbors import KNeighborsClassifier, KNeighborsRegressor
#if socket.gethostname() == "kusanagi":
#	device = t.device("cuda:2")
#else:
#	device = t.device("cpu")
ERASE = False
CV_SETS = 5
COUNTRY_STRATIFICATION = True
TOY = False
GENOLIST_PATH = "../galiana/metadata/genoList.csv"
PHENOLIST_PATH = "../galiana/metadata/phenoList.csv"
STUDIESLIST_PATH = "../galiana/metadata/study_list.csv"
AT_REFGENE_PATH = "atdb/AT_refGene.txt"
AT_GENESLIST = "../galiana/marshalled/ATgeneslist.cPickle"
NATIONS = None#["Sweden", "Spain"]
MIN_NUM_SAMPLE_COUNTRY = 1
MIN_NUM_PHEN = 70
AT_GENOMES_MARSHAL_PATH = "../galiana/marshalled/ATgenomes.allRegions.allGenes.npy" 
AT_MARSHALLED_GENOMES_LIST = "../galiana/marshalled/ATgenomes.allRegions.allGenes.list.cPickle"
nT = 4
t.set_num_threads(nT)
t.set_num_interop_threads(nT)
print(t.get_num_threads(), t.get_num_interop_threads())
selectedPhen = []#["Fe57", "Zn66", "Cu65", "Mn55", "Ca43", "Anthocyanin 22", "Anthocyanin 16", "Anthocyanin 10", "GR21 warm", "Seed bank 133-91", "Storage 56 days", "GR21", "GR63 warm", "GR105 cold", "GR105 warm", "GR21 cold", "Storage 28 days", "Storage 7 days", "GR63 cold", "Seed Dormancy", "Fe57", "Zn66", "Cu65", "Mn55", "Ca43"]

def main(args):
	if not len(args) == 3:
		print ("\nUSAGE: python ionome.py cuda:2 runName\n")
		return 1
	device = args[1]
	name = args[2]
	os.system("mkdir -p " +name)
	dataIons, ionsList = parseIonomeComplete("dataset/selected_Ion_data.csv")
	DATA = ATsamples(GENOLIST_PATH, ionsList, dataIons, NATIONS, MIN_NUM_SAMPLE_COUNTRY, AT_REFGENE_PATH, AT_GENOMES_MARSHAL_PATH, AT_MARSHALLED_GENOMES_LIST, AT_GENESLIST,  MIN_NUM_PHEN)
	DATA.scaleLabels(StandardScaler)
	IDS = list(DATA.genomes.keys())
	cv = 0
	totYp = []
	totY = []
	totCorresp = []
	totNation = []
	print("COUNTRY STRATIFICATION IS ", COUNTRY_STRATIFICATION)
	if COUNTRY_STRATIFICATION:
		groups, IDS, countriesCorresp = DATA.countryBasedSplitSklearn()
		kf = GroupKFold(n_splits = CV_SETS)
		SPLITS = kf.split(IDS, groups=groups)
	else:
		IDS = list(DATA.genomes.keys())
		kf = KFold(CV_SETS, shuffle = True)
		SPLITS = kf.split(IDS)
	#adj = readPPIsSparseTensor("../galiana/PPI/DeBodt_predicted_PPI.txt", DATA.ATgenes)

	#adj = readPPIsSparseTensor("PPI/DeBodt_predicted_PPI.txt", DATA.ATgenes)
	Sg = {}
	Sx = {}
	for trainIds, testIds in SPLITS:
		print ("Working on CV set %d/%d ======================" % (cv+1, CV_SETS))
		X, Y, corresp = buildVectors(trainIds, DATA)
		##np.random.shuffle(Y)
		##for s in X:
		##	np.random.shuffle(s)
		#Xscaler = CountryScaler(StandardScaler, DATA.nations)
		#X = Xscaler.fit_transform(X, corresp, DATA.genomes)
		X = t.deg2rad(t.from_numpy(X))
		X = t.cat([t.sin(X[:,0]).view(-1,1), t.sin(X[:,1]).view(-1,1), t.cos(X[:,1]).view(-1,1)], dim=1)
		#Xscaler = U.GeneNormalizer()
		#Xscaler = StandardScaler()
		#Xscaler = U.PerGeneScaler(StandardScaler())
		#X = Xscaler.fit_transform(X.numpy())
		#print list(X[0,:,17])
		#print X[0,:,-1].shape
		#raw
		assert len(X) == len(Y)
		print ("Built %d vectors of len %d" % (len(X), len(X[0])))
		#model = GRN.BaselineALL(len(PAM.variantTypesHT), len(DATA.ATgenes), len(DATA.ionsList[:]), name= name )
		model = GRN.GpsNN( len(DATA.ionsList[:]), name= name) 
		#model = GRN.BaselineTransposed(len(PAM.variantTypesHT), len(DATA.ATgenes), len(DATA.ionsList[:]), name= name )
		#model = GRN.AttentionALL(len(PAM.variantTypesHT)+1, len(DATA.ATgenes), len(DATA.ionsList[:]), name= name )
		model.to(device)
		wrapper = GRN.NNwrapper(model)
		#wrapper = KNeighborsRegressor(1)
		wrapper.fit(X, Y, device, epochs=50, batch_size = 10, LOG=False)
		#wrapper.fit(X, Y)
		#Yp = wrapper.predict(X)
		Yp = wrapper.predict(X, device)
		#U.computeMultiRegrPerCountry(DATA, corresp,  Yp, Y, IGNORE_INDEX, DATA.phenotypesList[:], DATA.phenotypes, None, "test."+name+".cv"+str(cv+1))
		U.computeScoresMultiRegr(Yp, Y, IGNORE_INDEX, DATA.ionsList[:], None, None)#"train."+name+".cv"+str(cv+1))

		del X, Yp, trainIds, corresp
		print ("Testing...")

		x, y, corresp = buildVectors(testIds, DATA)
		##for s in x:
		##	np.random.shuffle(s)
		##np.random.shuffle(y)
		x = t.deg2rad(t.from_numpy(x))
		x = t.cat([t.sin(x[:,0]).view(-1,1), t.sin(x[:,1]).view(-1,1), t.cos(x[:,1]).view(-1,1)], dim=1)
		#x = Xscaler.transform(x.numpy())
		assert len(x) == len(y)
		print ("Built %d vectors of len %d" % (len(x), len(x[0])))
		yp = wrapper.predict(x, device, batch_size=12)
		#yp = wrapper.predict(x)
		#U.computeMultiRegrPerCountry(DATA, corresp, yp, y, IGNORE_INDEX, DATA.phenotypesList[:], DATA.phenotypes, None, "test."+name+".cv"+str(cv+1))
		#sal = SaliencyWrapper(model)
		#Sg, Sx = sal.predictSALperSample(x, y, corresp, device, (Sg, Sx), DATA.ATgenes)

		U.computeScoresMultiRegr(yp, y, IGNORE_INDEX, DATA.ionsList[:], None, None)# "test."+name+".cv"+str(cv+1))

		totYp += list(yp)
		totY += list(y)
		totCorresp += corresp
		for cor in corresp:
			totNation.append(DATA.genomes[cor]["country"])
		cv += 1
	assert len(totYp) == len(totCorresp) == len(totNation)
	print ("FINAL AVG:")
	U.computeMultiRegrPerCountry(DATA, totCorresp, totYp, totY, IGNORE_INDEX, DATA.ionsList[:], None, name+"/all."+name+".results")

	U.computeScoresMultiRegr(np.array(totYp), np.array(totY), IGNORE_INDEX, DATA.ionsList[:], None, name+"/all."+name+".resultsTotal.cv"+str(CV_SETS)+".txt")
	pickle.dump((corresp, totYp, totY, totNation), open(name+"/all."+name+".preds.cv"+str(CV_SETS)+".m","wb"))
	pickle.dump(DATA.ionsList, open(name+"/ionsList.cPickle", "wb"))
	#cPickle.dump(Sg, open(name+"/finalALL."+name+".saliencyGene.m","w"))
	#cPickle.dump(Sx, open(name+"/finalALL."+name+".saliencyX.m","w"))
	print( "Results stored in folder %s/" % name)
	return 0

def buildVectors(ids, DATA, OLD = False):
	X = []
	Y = []
	corresp = []
	for i, pos in enumerate(ids):
		genomeID = DATA.ids[pos]
		X.append(DATA.genomes[genomeID]["gps"])
		Y.append(DATA.genomeAnnotation[genomeID])
		corresp.append(genomeID)
	return np.array(X, dtype=np.float32), np.array(Y), corresp

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
