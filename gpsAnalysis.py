#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  readGenoList.py
#  
#  Copyright 2019 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import sources.GraphConv as GRN
from sklearn.metrics import matthews_corrcoef, accuracy_score, classification_report, f1_score
from sources.readPPI import readPPIsSparseTensor
import csv, os, sys, pickle
import numpy as np
import torch as t
import sources.utils as U
import parseAnnovarMultianno as PAM
import socket
from sklearn.preprocessing import StandardScaler, RobustScaler, MinMaxScaler, MaxAbsScaler
from sklearn.model_selection import KFold
from sources.ATsamples import ATsamples, IGNORE_INDEX
from sources.readIonome import parseIonomeComplete
#if socket.gethostname() == "kusanagi":
#	device = t.device("cuda:2")
#else:
#	device = t.device("cpu")
ERASE = False
CV_SETS = 5
TOY = False
GENOLIST_PATH = "../galiana/metadata/genoList.csv"
PHENOLIST_PATH = "../galiana/metadata/phenoList.csv"
STUDIESLIST_PATH = "../galiana/metadata/study_list.csv"
AT_REFGENE_PATH = "atdb/AT_refGene.txt"
AT_GENESLIST = "../galiana/marshalled/ATgeneslist.cPickle"
NATIONS = None#["Sweden", "Spain"]
MIN_NUM_SAMPLE_COUNTRY = 1
MIN_NUM_PHEN = 70
AT_GENOMES_MARSHAL_PATH = "../galiana/marshalled/ATgenomes.allRegions.allGenes.npy" 
AT_MARSHALLED_GENOMES_LIST = "../galiana/marshalled/ATgenomes.allRegions.allGenes.list.cPickle"
nT = 4
t.set_num_threads(nT)
t.set_num_interop_threads(nT)
print(t.get_num_threads(), t.get_num_interop_threads())
selectedPhen = []#["Fe57", "Zn66", "Cu65", "Mn55", "Ca43", "Anthocyanin 22", "Anthocyanin 16", "Anthocyanin 10", "GR21 warm", "Seed bank 133-91", "Storage 56 days", "GR21", "GR63 warm", "GR105 cold", "GR105 warm", "GR21 cold", "Storage 28 days", "Storage 7 days", "GR63 cold", "Seed Dormancy", "Fe57", "Zn66", "Cu65", "Mn55", "Ca43"]

def main(args):
	if not len(args) == 3:
		print ("\nUSAGE: python ionome.py cuda:2 runName\n")
		return 1
	device = args[1]
	name = args[2]
	os.system("mkdir -p " +name)
	dataIons, ionsList = parseIonomeComplete("dataset/selected_Ion_data.csv")
	DATA = ATsamples(GENOLIST_PATH, ionsList, dataIons, NATIONS, MIN_NUM_SAMPLE_COUNTRY, AT_REFGENE_PATH, AT_GENOMES_MARSHAL_PATH, AT_MARSHALLED_GENOMES_LIST, AT_GENESLIST,  MIN_NUM_PHEN)
	DATA.scaleLabels(StandardScaler)
	IDS = list(DATA.genomes.keys())
	cv = 0
	totYp = []
	totY = []
	totCorresp = []
	totNation = []
	kf = KFold(CV_SETS, shuffle = True)
	#adj = readPPIsSparseTensor("../galiana/PPI/DeBodt_predicted_PPI.txt", DATA.ATgenes)

	#adj = readPPIsSparseTensor("PPI/DeBodt_predicted_PPI.txt", DATA.ATgenes)
	Sg = {}
	Sx = {}
	for trainIds, testIds in kf.split(IDS):
		print ("Working on CV set %d/%d ======================" % (cv+1, CV_SETS))
		X, Y, corresp = buildVectors(list(trainIds)+list(testIds), DATA)
		pickle.dump((X, Y, corresp), open("gpsLabel.pickle","wb"))
		return 0

def buildVectors(ids, DATA, OLD = False):
	X = []
	Y = []
	corresp = []
	for i, pos in enumerate(ids):
		genomeID = DATA.ids[pos]
		X.append(DATA.genomes[genomeID]["gps"])
		Y.append(DATA.genomeAnnotation[genomeID])
		corresp.append(genomeID)
	return np.array(X, dtype=np.float32), np.array(Y), corresp

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
