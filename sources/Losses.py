import torch as t
from torch.autograd import Variable

class LossWrapper():

	"""
	Class that wraps pytorch LogitBCE allowing for ignore index.
	"""

	def __init__(self, loss, ignore_index = -1):
		self.loss = loss
		self.ignore_index = ignore_index
		print( " >>> IGNORE INDEX:" , self.ignore_index)
	
	def __call__(self, input, target):
		input = input.view(-1)
		target = target.view(-1)
		mask = target.ne(self.ignore_index)
		input = t.masked_select(input, mask)
		target = t.masked_select(target, mask)
		return self.loss(input, target)

class CombinedLoss(t.nn.Module):

	def __init__(self, lossList, lossWeights, type):
		assert len(lossList) == len(lossWeights) or len(lossWeights) == 0
		self.lossList = lossList
		self.lossWeights = lossWeights
		if len(lossWeights) == 0:
			print( "No loss weight applied!")
		self.type = type
	
	def __call__(self, input, target):
		i = 0
		tot = 0
		while i < target.size()[1]:
			tmp =  self.lossList[i](input[:,i], target[:,i])
			if not t.isnan(tmp):
				if len(self.lossWeights) != 0:
					tot += tmp*self.lossWeights[i]
				else:
					tot += tmp
			i += 1
		return tot

class PearsonLoss(t.nn.Module):

	def __init__(self, ignore_index = -1, negative=False):
		self.ignore_index = ignore_index
		self.type = "regression"
		self.negative = negative

	def __call__(self, input, target):
		return self.forward(input, target)

	def forward(self, input, target):
		#print "qui", input.size(), target.size()
		input = input.view(-1)
		target = target.view(-1)
		mask = target.ne(self.ignore_index)
		input = t.masked_select(input, mask)
		target = t.masked_select(target, mask)
		#print "qu", input.size(), target.size()
		vx = input - t.mean(input)
		vy = target - t.mean(target)
		#print vx, vy
		cost = t.sum(vx * vy) / (t.sqrt(t.sum(vx ** 2)) * t.sqrt(t.sum(vy ** 2)))
		if self.negative:
			cost = -1 * cost
		return cost

