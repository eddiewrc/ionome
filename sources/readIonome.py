import numpy as np

def parseIonome(f):
	ifp = open(f)
	dleaf = {}
	dseed = {}
	lines = ifp.readlines()
	ionsList = lines.pop(0).strip().replace("\"","").split(",")[9:]
	ifp.close()
	for l in lines:
		tmp = l.strip().replace("\"","").split(",")
		#print tmp
		tmp1 = {}
		tmp1["country"] = tmp[3]
		tmp1["ions"] = parseIons(tmp[9:])
		tmp1["gps"] = (float(tmp[4]),float(tmp[5]))
		if "seed" in tmp[7]:
			dseed[tmp[1]] = tmp1
		elif "leaf" in tmp[7]:
			dleaf[tmp[1]] = tmp1
	print ("Ions: ", ionsList, len(ionsList))
	print ("Read %d seed and %d leaf samples" % (len(dseed), len(dleaf)))
	return dleaf, dseed, ionsList #{dseed={ATid:{country:, ions:[values], gps:(coord1, c2)}}}

def parseIonomeComplete(f):
	ifp = open(f)
	dleaf = {}
	dseed = {}
	lines = ifp.readlines()
	ionsListTmp = lines.pop(0).strip().replace("\"","").split(",")[9:]
	ionsList = []
	for i in ionsListTmp:
		ionsList.append(i+"_seed")
	for i in ionsListTmp:
		ionsList.append(i+"_leaf")
	ifp.close()
	names = set()
	for l in lines:
		tmp = l.strip().replace("\"","").split(",")
		#print tmp
		tmp1 = {}
		tmp1["country"] = tmp[3]
		tmp1["ions"] = parseIons(tmp[9:])
		tmp1["gps"] = (float(tmp[4]),float(tmp[5]))
		if "seed" in tmp[7]:
			dseed[tmp[1]] = tmp1
		elif "leaf" in tmp[7]:
			dleaf[tmp[1]] = tmp1
		names.add(tmp[1])
	db = {}
	for n in names:
		if n not in dseed or n not in dleaf:
			continue
		db[n] = {"country":dseed[n]["country"], "gps":dseed[n]["gps"], "ions":np.concatenate([dseed[n]["ions"],dleaf[n]["ions"]])}
	print("Found %d annotated samples"%len(db))
	print ("Ions: ", ionsList, len(ionsList))
	return db, ionsList #{dseed={ATid:{country:, ions:[values], gps:(coord1, c2)}}}

def parseIons(l):
	v = []
	for i in l:
		v.append(float(i))
	return np.array(v, dtype=np.float32)

def main(args):
	parseIonome("dataset/selected_Ion_data.csv")



if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
