#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pred1.py
#  
#  Copyright 2018 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os, copy, random, time, math, socket
from sys import stdout
import numpy as np
import torch as t
from torch.autograd import Variable
from torch.nn.utils.rnn import PackedSequence, pack_padded_sequence, pad_packed_sequence
from torch.utils.data import Dataset, DataLoader
from scipy.stats import pearsonr
import sources.Losses as L
from sources.ResNets import LeakyHardTanh, ResidualBlock
#t.manual_seed(0)

class GpsNN(t.nn.Module):
	def __init__(self, numOut, name = "NN"):
		super(GpsNN, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		self.freezed = False
		self.nn = t.nn.Sequential( t.nn.Linear(3, 500), t.nn.LayerNorm(500), t.nn.LeakyReLU())
		self.final = t.nn.Sequential( t.nn.Linear(500, numOut))
		#self.final = t.nn.Sequential(t.nn.Dropout(0.1), t.nn.Linear(self.numGenes, 10), t.nn.LeakyReLU(), t.nn.Linear(10,1))
		self.apply(init_weights)
		
	def forward(self, x, GET_ACT = False):	
		#print x.size()	
		o1 = self.nn(x)
		#print o.size()
		o = self.final(o1.squeeze())
		#o = (t.sum(o.squeeze(), 1)/o.size(1)).unsqueeze(1)		
		if GET_ACT:
			return o1, o
		else:
			return o

class GraphConvolution(t.nn.Module):
	
	def __init__(self, in_features, out_features, adjMatrix, bias=True):
		super(GraphConvolution, self).__init__()
		self.adj = adjMatrix
		self.in_features = in_features
		self.out_features = out_features
		self.weight = t.nn.Parameter(t.FloatTensor(in_features, out_features))
		if bias:
			self.bias = t.nn.Parameter(t.FloatTensor(out_features))
		else:
			self.register_parameter('bias', None)
		self.reset_parameters()

	def reset_parameters(self):
		stdv = 1. / math.sqrt(self.weight.size(1))
		self.weight.data.uniform_(-stdv, stdv)
		if self.bias is not None:
			self.bias.data.uniform_(-stdv, stdv)

	def forward(self, input):
		batch_size = input.size()[0]
		#print input.size()
		support = t.matmul(input.squeeze(), self.weight)
		#print support.size()
		output = t.stack([t.sparse.mm(self.adj, support[i,:].float()) for i in range(batch_size)])
		#print output.size()
		#output = t.mm(self.adj, support)
		if self.bias is not None:
			return output + self.bias
		else:
			return output

class AttentionALL(t.nn.Module):
	def __init__(self, genesize, numGenes, numOut, name = "NN"):
		super(AttentionALL, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		print ("Building adj...")
		#self.adj = createSymmNormAdjSparse(adj, device)
		#print self.adj.size()
		self.numOut = numOut
		self.geneInputSize = genesize
		self.numGenes = numGenes
		DROPOUT= 0.1
		ACTa = t.nn.Tanh()
		self.inputTransformP = t.nn.Sequential( t.nn.Linear(self.geneInputSize, 100), t.nn.LayerNorm(100), t.nn.Tanh(), t.nn.Linear(100, numOut))
		self.inputTransformA = t.nn.Sequential( t.nn.Linear(self.geneInputSize, 50), t.nn.LayerNorm(50), ACTa, t.nn.Linear(50, 1))

		
		#self.middleGcnA = t.nn.Sequential(t.nn.LayerNorm(5), t.nn.Tanh(), GraphConvolution(5, 10, self.adj), t.nn.Tanh(), GraphConvolution(10, numOut, self.adj), t.nn.Softmax(dim=1))
		self.middleA = t.nn.Sequential(t.nn.Dropout(DROPOUT), t.nn.LayerNorm(numGenes), ACTa, t.nn.Linear(self.numGenes, 50), t.nn.LayerNorm(50), ACTa, t.nn.Linear(50, numGenes*numOut))
		self.s = t.nn.Softmax(dim=1)
		#TODO: put a graph conv as middle

		self.apply(init_weights)
		
	def forward(self, x):	
		batch_size = x.size()[0]
		p = self.inputTransformP(x).transpose(1,2)
		a = self.inputTransformA(x).squeeze()
		#print a.size(), p.size()
		a = self.middleA(a).view(batch_size, self.numGenes, self.numOut)
		#print a.size(), p.size()
		a = self.s(a)
		#print t.sum(a, dim=1)
		#print t.sum(a,dim=1).size()
		#raw
		o = t.bmm(p,a)
		#print o.size()
		o = o.diagonal(dim1=1, dim2=2).contiguous()
		#print o.size()
		#raw
		return o

class BaselineALL(t.nn.Module):
	def __init__(self, genesize, numGenes, numOut, name = "NN", transf=False, freezed=False):
		super(BaselineALL, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		self.freezed = freezed
		
		self.geneInputSize = genesize
		self.numGenes = numGenes
		DROPOUT= 0.2
		self.inputTransform = t.nn.Sequential(t.nn.Dropout(DROPOUT), t.nn.Linear(self.geneInputSize, 50), t.nn.LayerNorm(50), t.nn.Tanh(), t.nn.Linear(50, 1))
		if not transf:
			self.middle = t.nn.Sequential(t.nn.LayerNorm(numGenes), t.nn.Tanh(), t.nn.Linear(self.numGenes, 50), t.nn.LayerNorm(50), t.nn.Tanh(), t.nn.Dropout(DROPOUT), t.nn.Linear(50, 50), t.nn.LayerNorm(50), t.nn.Tanh())#
		else:
			self.middle = t.nn.Sequential(t.nn.LayerNorm(numGenes), t.nn.Tanh(), t.nn.Linear(self.numGenes, 50), t.nn.LayerNorm(50), t.nn.Tanh(), t.nn.Dropout(DROPOUT), t.nn.Linear(50, 50), t.nn.LayerNorm(50), t.nn.Tanh())
		#self.middle = t.nn.Sequential(t.nn.Dropout(0.2), t.nn.Linear(self.numGenes, 100), t.nn.LayerNorm(100), t.nn.Tanh(), t.nn.Dropout(0.2), ResidualBlock(t.nn.Linear(100, 100)), t.nn.LayerNorm(100), t.nn.Tanh())
		if not freezed:
			self.phenotypesPred = t.nn.Sequential(t.nn.Dropout(DROPOUT), t.nn.Linear(50, numOut))
		else:
			self.phenotypesPred = t.nn.Sequential(t.nn.Linear(50, 200), t.nn.LayerNorm(200), t.nn.Tanh(), t.nn.Linear(200, numOut))
		#self.apply(init_weights)
		
	def forward(self, x):	
		batch_size = x.size()[0]
		o = self.inputTransform(x).view(batch_size, -1)
		o = self.middle(o)
		po = self.phenotypesPred(o)
		#print po.size()
		#raw
		return po

def init_weights( m):
	if isinstance(m, t.nn.Conv1d) or isinstance(m, t.nn.Linear) or isinstance(m, t.nn.Bilinear) or isinstance(m, GraphConvolution):
		print ("Initializing weights...", m.__class__.__name__)
		#t.nn.init.normal(m.weight, 0, 0.01)
		t.nn.init.xavier_uniform(m.weight)
		m.bias.data.fill_(0.1)
	elif isinstance(m, t.nn.Embedding):
		print ("Initializing weights...", m.__class__.__name__)
		t.nn.init.xavier_uniform(m.weight)	
			
class BaselineTransposed(t.nn.Module):
	def __init__(self, genesize, numGenes, numOut, name = "NN"):
		super(BaselineTransposed, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		self.freezed = False
		self.geneInputSize = genesize
		self.numGenes = numGenes
		DROPOUT= 0.2
		self.inputTransform = t.nn.Sequential(t.nn.Dropout(DROPOUT), t.nn.Linear(self.geneInputSize, 200), t.nn.LayerNorm(200), t.nn.Tanh(), t.nn.Linear(200, 2000))
		#self.parse = t.nn.Sequential(t.nn.Conv1d(numGenes, 1, 1))
		self.middle = t.nn.Sequential(t.nn.LayerNorm(2000), t.nn.Tanh(), t.nn.Linear(2000, 100), t.nn.LayerNorm(100), t.nn.Tanh(), t.nn.Dropout(DROPOUT), t.nn.Linear(100, 50), t.nn.LayerNorm(50), t.nn.Tanh())
		#self.middle = t.nn.Sequential(t.nn.Dropout(0.2), t.nn.Linear(self.numGenes, 100), t.nn.LayerNorm(100), t.nn.Tanh(), t.nn.Dropout(0.2), ResidualBlock(t.nn.Linear(100, 100)), t.nn.LayerNorm(100), t.nn.Tanh())
		self.phenotypesPred = t.nn.Sequential(t.nn.Linear(50, numOut))
		#self.apply(init_weights)
		
	def forward(self, x):	
		batch_size = x.size()[0]
		#x = x + t.randn(x.size(), dtype=t.float, device=x.device).normal_() * 0.1 #gaussian noise with 0 mean and sigma std
		o = self.inputTransform(x)
		#print (o.size())
		#o = self.parse(o).squeeze()
		o = t.mean(o, dim=1)
		
		#print (o.size())
		#raw
		o = self.middle(o)
		po = self.phenotypesPred(o)
		return po

class ATDataset(Dataset):
    
	def __init__(self, X, Y):	
	
		assert len(Y) == len(X)		
		self.X = t.tensor(X, dtype=t.float32)
		self.Y = t.tensor(Y, dtype=t.float32)
		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx], self.Y[idx]

class ATpredDataset(Dataset):
    
	def __init__(self, X):	
		self.X = t.tensor(X, dtype=t.float32)
		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx]

class NNwrapper():

	def __init__(self, model):
		if type(model) == str:
			self.model = t.load(model)
		else:
			self.model = model

	def reg(self, l1List, l2List):
		l2 = 0
		l1 = 0
		for m in l1List:
			for w in m.parameters():
				l1 += w.norm(1)
		for m in l2List:
			for w in m.parameters():
				l2 += w.norm(2)
		return l1, l2
	
	def fit(self, originalX, originalY, device, epochs = 50, batch_size=20, save_model_every=10, weight_decay = 1e-6, learning_rate = 1e-2, silent = False, IGNORE_INDEX = -9999, LOG=False):
		########DATASET###########
		t1 = time.time()
		dataset = ATDataset(originalX, originalY)
		t2 = time.time()
		print ("Dataset created in %.2fs" % (t2 - t1))
		#######MODEL##############		
		self.model.train()	
		print ("Start training")
		########LOSS FUNCTION######
		#lossfn = L.LossWrapper(t.nn.L1Loss(), ignore_index=IGNORE_INDEX)#MSE prob best
		lossfn = L.LossWrapper(t.nn.MSELoss(), ignore_index=IGNORE_INDEX)
		########OPTIMIZER##########	
		if self.model.freezed:
			parameters = list(self.model.phenotypesPred.parameters())	
		else:
			parameters =list(self.model.parameters())
		p = []
		for i in parameters:
			p+= list(i.data.cpu().numpy().flat)
		print ('Number of parameters=',len(p))
		del p        
		self.model.train()
		print ("Training mode: ", self.model.training)

		#parameters = list(self.model.g.parameters())+list(self.model.final2.parameters())+list(self.model.pathLayer.parameters())
		if self.model.freezed:
			print("Optimizer freezed")
			optimizer = t.optim.RMSprop(self.model.phenotypesPred.parameters(), lr=learning_rate, weight_decay=weight_decay)
		else:
			optimizer = t.optim.RMSprop(self.model.parameters(), lr=learning_rate, weight_decay=weight_decay)
		scheduler = t.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.3, patience=1, verbose=True, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08)
		
		########DATALOADER#########
		t1 = time.time()
		loader = DataLoader(dataset, batch_size=batch_size, shuffle=True, sampler=None, num_workers=0)
		t2 = time.time()
		print ("Dataloader created in %.2fs" % (t2 - t1))
		print ("Start epochs")
		e = 1
		minLoss = 1000000000
		while e < epochs:			
			errTot = 0
			i = 1
			start = time.time()
			for sample in loader:
				optimizer.zero_grad()
				x, y = sample
				x = x.to(device)
				y = y.to(device)
				#print( y.size())
				yp = self.model.forward(x)
				#l1, l2 = self.reg(self.model.moduleList, [self.model.inputTransform, self.model.predPhen])
				#print(yp.size())
				loss = lossfn(yp, y)# + l1*1e-3 + l2*1e-3
				loss.backward()	
				optimizer.step()
				errTot += loss.data
				i+=batch_size						
				perc = (100*i/float(len(dataset))	)	
				#stdout.write("\nepoch=%d %d (%3.2f%%), errBatch=%f" % (e, i, perc, loss.data[0]))
				#stdout.flush()				
			end = time.time()	
			if not silent:				
				print (" epoch %d, ERRORTOT: %f (%fs)" % (e, errTot, end-start))
			scheduler.step(errTot)
			if e % save_model_every == 0:
				print ("Store model ", e)
				#t.save(self.model, "models/"+self.model.name+".iter_"+str(e)+".t")							
			stdout.flush()	
			e += 1	
		if LOG:
			self.logger.shutdown()
		#t.save(self.model, "models/"+self.model.name+".final.t")
	
	def predict(self, X, device, batch_size=10):
		self.model.eval()
		print ("Predicting...")
		dataset = ATpredDataset(X)
		loader = DataLoader(dataset, batch_size=batch_size, shuffle=False, sampler=None, num_workers=0)
		preds1 = []
		act = []
		for sample in loader:
			x = sample
			x = x.to(device)
			#print(x.size())
			y_pred = self.model.forward(x)
			preds1 += y_pred.data.squeeze().tolist()			
		return np.array(preds1)

	def predictInterpret(self, X, device, interpreter, baseline, target):
		self.model.eval()
		print ("Predicting...")
		dataset = ATpredDataset(X)
		loader = DataLoader(dataset, batch_size=10, shuffle=False, sampler=None, num_workers=0)
		preds1 = []
		attributions = []
		act = []
		for sample in loader:
			x = sample
			x = x.to(device)
			y_pred = self.model.forward(x)
			attr, _ = interpreter.attribute(x, baseline, target=target, return_convergence_delta=True)
			attributions += attr.detach().to("cpu").tolist()
			preds1 += y_pred.data.to("cpu").squeeze().tolist()			
		return np.array(preds1), attributions
		

if __name__ == '__main__':
	from iongreen2 import main
	main()
	#mainEVAL()
