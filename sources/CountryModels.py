#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pred1.py
#  
#  Copyright 2018 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import matplotlib.pyplot as plt
import os, copy, random, time, math, socket
from sys import stdout
import numpy as np
import torch as t
from torch.utils.data import Dataset, DataLoader
from scipy.stats import pearsonr
#t.manual_seed(0)

def init_weights( m):
	if isinstance(m, t.nn.Conv1d) or isinstance(m, t.nn.Linear) or isinstance(m, t.nn.Bilinear) :
		print ("Initializing weights...", m.__class__.__name__)
		#t.nn.init.normal(m.weight, 0, 0.01)
		t.nn.init.xavier_uniform(m.weight)
		m.bias.data.fill_(0.1)
	elif isinstance(m, t.nn.Embedding):
		print ("Initializing weights...", m.__class__.__name__)
		t.nn.init.xavier_uniform(m.weight)	

class BaselineNN(t.nn.Module):
	def __init__(self, genesize, numGenes, numOut, name = "NN"):
		super(BaselineNN, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		
		self.geneInputSize = genesize
		self.numGenes = numGenes
		
		self.nn = t.nn.Sequential( t.nn.Linear(self.geneInputSize, 1), t.nn.LeakyReLU())
		self.final = t.nn.Sequential(t.nn.Linear(self.numGenes, 100), t.nn.LeakyReLU(), t.nn.Linear(100, numOut), t.nn.LeakyReLU())
		#self.final = t.nn.Sequential(t.nn.Dropout(0.1), t.nn.Linear(self.numGenes, 10), t.nn.LeakyReLU(), t.nn.Linear(10,1))
		self.apply(init_weights)
		
	def forward(self, x, GET_ACT = False):	
		#print x.size()	
		o1 = self.nn(x)
		#print o.size()
		o = self.final(o1.squeeze())
		#o = (t.sum(o.squeeze(), 1)/o.size(1)).unsqueeze(1)		
		if GET_ACT:
			return o1, o
		else:
			return o
	
class PredictFromLabelNN(t.nn.Module):
	def __init__(self, featSize, numOut, name = "NN"):
		super(PredictFromLabelNN, self).__init__()		
		os.system("mkdir -p models")
		self.name = name
		self.featSize = featSize	
		
		self.final = t.nn.Sequential(t.nn.Linear(self.featSize, 200), t.nn.LeakyReLU(), t.nn.Linear(200, numOut), t.nn.LeakyReLU())
		#self.final = t.nn.Sequential(t.nn.Dropout(0.1), t.nn.Linear(self.numGenes, 10), t.nn.LeakyReLU(), t.nn.Linear(10,1))
		self.apply(init_weights)
		
	def forward(self, x, GET_ACT = False):	
		o = self.final(x)
		#o = (t.sum(o.squeeze(), 1)/o.size(1)).unsqueeze(1)		
		if GET_ACT:
			return o1, o
		else:
			return o		

class ATDataset(Dataset):
    
	def __init__(self, X, Y):	
	
		assert len(Y) == len(X)		
		self.X = t.tensor(X, dtype=t.float32)
		self.Y = t.tensor(Y, dtype=t.long)
		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx], self.Y[idx]

class ATpredDataset(Dataset):
    
	def __init__(self, X):	
		self.X = t.tensor(X, dtype=t.float32)
		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx]

class NNwrapper():

	def __init__(self, model):
		if type(model) == str:
			self.model = t.load(model)
		else:
			self.model = model

	def fit(self, originalX, originalY, device, epochs = 50, batch_size=20, save_model_every=10, weight_decay = 1e-5, learning_rate = 1e-2, silent = False, IGNORE_INDEX = -9999, LOG=False):
		########DATASET###########
		t1 = time.time()
		dataset = ATDataset(originalX, originalY)
		t2 = time.time()
		print ("Dataset created in %.2fs" % (t2 - t1))
		#######MODEL##############		
		self.model.train()	
		print ("Start training")
		########LOSS FUNCTION######
		lossfn = t.nn.CrossEntropyLoss()
		#lossfn = L.LossWrapper(t.nn.SmoothL1Loss(), ignore_index=IGNORE_INDEX)
		########OPTIMIZER##########	
		parameters =list(self.model.parameters())
		p = []
		for i in parameters:
			p+= list(i.data.cpu().numpy().flat)
		print ('Number of parameters=',len(p))
		del p        
		self.model.train()
		print ("Training mode: ", self.model.training)

		#parameters = list(self.model.g.parameters())+list(self.model.final2.parameters())+list(self.model.pathLayer.parameters())
		optimizer = t.optim.Adam(parameters, lr=learning_rate, weight_decay=weight_decay)
		scheduler = t.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.3, patience=3, verbose=True, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08)
		
		########DATALOADER#########
		t1 = time.time()
		loader = DataLoader(dataset, batch_size=batch_size, shuffle=True, sampler=None, num_workers=0)
		t2 = time.time()
		print ("Dataloader created in %.2fs" % (t2 - t1))

		print( "Start epochs")
		e = 1
		minLoss = 1000000000
		while e < epochs:			
			errTot = 0
			i = 1
			start = time.time()
			for sample in loader:
				optimizer.zero_grad()
				x, y = sample
				x = x.to(device)
				y = y.to(device)
				#print y
				yp = self.model.forward(x)
				loss = lossfn(yp, y)
				loss.backward()	
				optimizer.step()
				errTot += loss.data
				i+=batch_size						
				perc = (100*i/float(len(dataset))	)	
				#stdout.write("\nepoch=%d %d (%3.2f%%), errBatch=%f" % (e, i, perc, loss.data[0]))
				#stdout.flush()				
			end = time.time()	
			if not silent:				
				print (" epoch %d, ERRORTOT: %f (%fs)" % (e, errTot, end-start))
			scheduler.step(errTot)
			if e % save_model_every == 0:
				print( "Store model ", e)
				#t.save(self.model, "models/"+self.model.name+".iter_"+str(e)+".t")							
			stdout.flush()	
			e += 1	
		if LOG:
			self.logger.shutdown()
		#t.save(self.model, "models/"+self.model.name+".final.t")
	
	def predict(self, X, device):
		self.model.eval()
		print ("Predicting...")
		softmax = t.nn.Softmax()
		dataset = ATpredDataset(X)
		loader = DataLoader(dataset, batch_size=len(X), shuffle=False, sampler=None, num_workers=0)
		preds1 = []
		act = []
		for sample in loader:
			x = sample
			x = x.to(device)
			y_pred = softmax(self.model.forward(x))
			preds1 += y_pred.data.squeeze().tolist()			
		return np.array(preds1)
		
		

if __name__ == '__main__':
	from iongreen2 import main
	main()
	#mainEVAL()
