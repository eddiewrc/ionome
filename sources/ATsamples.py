
import parseAnnovarMultianno as PAM
import sources.parseMetaData as PMD
import sources.joinAnnotations as JA
import os, pickle
import numpy as np
ERASE = False
IGNORE_INDEX = -9999

class ATsamples(object):

	"""
	Class that is supposed to manage ALL the data issues in an incapsulated way.
	"""

	def __init__(self, GENOLIST_PATH, ionsList, dataIons, NATIONS_LIST, MIN_NUM_SAMPLE_COUNTRY, AT_REFGENE_PATH, AT_GENOMES_MARSHAL_PATH, AT_MARSHALLED_GENOMES_LIST, AT_GENESLIST, MIN_NUM_PHEN, selectedPhen = [], bannedPhen = []):
		"""
		Reads info about the genomes (name, country, collector, numPhen), the studies (name, desc, numPhen), the phenotypes (phenID, descr, growth cond.). 
		Reads the genome annotation (the phenotype values for each genome ID).
		Filters out the countries with less samples than MIN_NUM_SAMPLE_COUNTRY, and the correspondingenomes.
		
		"""
		genomes = PMD.readGenoList(GENOLIST_PATH) #{genomeID: {"name":name,"country":country, "collector":collector, "nPhen":phen}}
		if NATIONS_LIST == None or len(NATIONS_LIST) == 0:
			self.nations = self.getNumSamplesPerCountry(genomes, MIN_NUM_SAMPLE_COUNTRY)
		else:
			self.nations = {}
			for n in NATIONS_LIST:
				self.nations[n] = -1
		print ("## Initial %d genomes" % len(genomes))
		tmp = []
		for g in genomes.items():
			#if g[0] not in dataIons or g[1]["country"] not in self.nations.keys():
			if g[1]["nPhen"] == 0 or g[1]["country"] not in self.nations.keys():
				tmp.append( g[0])
		for g in tmp:
			del genomes[g]
		self.genomes = genomes
		del genomes
		print ("## After filtering: %d genomes" % len(self.genomes))
		self.genomeAnnotation = JA.integrateAnnotations(dataIons, self.genomes, MIN_NUM_PHEN) #{genomeID:[list of ions values]}
		########## filter out countries with few samples
		tmp = []
		for g in self.genomes.items():
			if not g[0] in self.genomeAnnotation:
				tmp.append(g[0])
		for g in tmp:
			del self.genomes[g]
		self.ids = list(self.genomes.keys())
		
		print ("Reading data from ",AT_GENOMES_MARSHAL_PATH )
		if AT_GENOMES_MARSHAL_PATH[-4:] == ".npy":
			self.genomeDB = np.load(AT_GENOMES_MARSHAL_PATH, allow_pickle=True)
		else:
			self.genomeDB = pickle.load(open(AT_GENOMES_MARSHAL_PATH, "rb"))
		self.genomesLookup = pickle.load(open(AT_MARSHALLED_GENOMES_LIST, "rb"))
		self.ATgenes = pickle.load(open(AT_GENESLIST, "rb"))
		self.ionsList = ionsList
		
	def getNumSamplesPerCountry(self, genomes, THR):
		"""
		Filters out the countries with less than THR samples.
		"""
		nations = {}
		for g in genomes.items():
			if not g[1]["country"] in nations:
				nations[g[1]["country"]] = 0
			nations[g[1]["country"]] += 1
		tmp = {}	
		for n in nations.items():
			if n[1] < THR:
				#print "Remove %s (%d samples)" % (n[0], n[1])
				continue
			tmp[n[0]] = n[1]
		nations = tmp
		print( "Remaining %d countries" % len(nations))
		print (sorted(nations.items(), key=lambda x:x[1]))
		return nations # {country:numSample}

	def countryBasedSplitSklearn(self):
		"""
		Returns the country-based clusters for KFold sklearn.
		"""
		clusters = []
		countries = {}
		countriesReverse = {}
		i = 0
		
		for g in self.genomes.items():
			co = g[1]["country"]
			if co in self.nations.keys():
				if not co in countries:
					countries[co] = i
					countriesReverse[i] = co
					i+=1
			else:
				raise Exception("this shoulndt be there")
		assert max(countries.values())+1 == len(countries)
		ids = []
		for i, g in enumerate(self.genomes.keys()):
			clusters.append(countries[self.genomes[g]["country"]])
			ids.append(g)
		assert ids == self.ids
		return clusters, ids, countriesReverse

	def getCountry(self, gid):
		"""
		Given a genome ID, returns the country
		"""
		assert gid in self.ids
		return self.genomes[gid]["country"]

	def getGenome(self, gid):
		"""
		Given a genome id, returns the genetic data
		"""
		assert gid in self.ids
		return self.genomeDB[self.genomesLookup[gid]]

	def scaleLabelsPerCountry(self):
		from sklearn.preprocessing import StandardScaler
		groups = {}
		corresp = []
		#assert genomeAnnotation.items() == genomeAnnotation.items()
		for i, gitem in enumerate(self.genomes.items()):
			g = gitem[1]
			gid = gitem[0]
			#print(gid, g)
			if not g["country"] in groups:
				groups[g["country"]] = [[],[]]
			groups[g["country"]][0].append(gid)
			groups[g["country"]][1].append(self.genomeAnnotation[gid])
			#print(self.genomeAnnotation[gid].shape)
			#input()
		for gr in groups.items():
			#print(gr)
			scaler = StandardScaler()
			y = scaler.fit_transform(np.array(gr[1][1]))
			#assert sum(np.mean(y,0)) < 0.0001
			#assert abs(np.mean(np.std(y,0)) -1) < 0.0001
			y = list(y)
			assert len(y) == len(gr[1][0])
			for	i, v in enumerate(y):
				self.genomeAnnotation[gr[1][0][i]] = v
		return self.genomeAnnotation


	def scaleLabels(self, scaler):
		LS = LabelScaler(scaler)
		self.genomeAnnotation, self.labelScaler = LS.fit_transform(self.ionsList, self.genomeAnnotation)

class LabelScaler:

	def __init__(self, scaler):
		self.scalerTemplate = scaler
		self.scalers = []

	def fit_transform(self, ionList, genomeAnnotation):
		Y = []
		newAnnotations = {}
		y = []
		corresp = []
		#assert genomeAnnotation.items() == genomeAnnotation.items()

		for i, annot in enumerate(genomeAnnotation.items()):
			val = annot[1]
			corresp.append(annot[0])
			y.append(val)
		#print corresp
		#print y
		scaler = self.scalerTemplate()
		y = scaler.fit_transform(np.array(y))
		#assert sum(np.mean(y,0)) < 0.0001
		#assert abs(np.mean(np.std(y,0)) -1) < 0.0001
		
		y = list(y)
		# self.scalers[0].mean_, self.scalers[0].var_#print y1
		i = 0
		for g in genomeAnnotation.items():
			assert g[0] == corresp[i]
			genomeAnnotation[g[0]] = y[i]
			i+=1
		return genomeAnnotation, scaler

