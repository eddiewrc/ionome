from GraphConv import ATDataset, NNwrapper
import Losses as L
import torch as t
from torch.utils.data import Dataset, DataLoader
import numpy as np
import sys
	
class SaliencyWrapper(NNwrapper, object):

	def __init__(self, model):
		super(SaliencyWrapper, self).__init__(model)
	
	def predictSALperSample(self, X, Y, corresp, device, saliency, genes, IGNORE_INDEX = -9999):
		saliencyG = saliency[0]
		saliencyX = saliency[1]
		self.model.eval()
		print "Predicting SAL..."
		dataset = ATDataset(X, Y)
		loader = DataLoader(dataset, batch_size=1, shuffle=False, sampler=None, num_workers=0)
		assert len(X) == len(Y) == len(corresp)
		i = 0
		for sample in loader:#runs only once
			x, y = sample
			sys.stdout.write("\rSample %d/%d" % (i+1, len(X)))
			x = x.to(device)
			x.requires_grad_()
			parameters = list(self.model.parameters())+ [x]
			optimizer = t.optim.Adam(parameters)#+[{"params":[x], "lr":1}])
			optimizer.zero_grad()
			y = y.to(device)
			yp = self.model.forward(x)
			#print yp.size()
			for phen in xrange(0, y.size()[1]):
				assert y.size()[0] == 1
				if y[0,phen] == IGNORE_INDEX:
					#print "skip ",phen
					continue
				#sys.stdout.write("\rphen %d/%d" % (phen, y.size()[1]))
				yp[0,phen].backward(retain_graph=True)
				if not saliencyG.has_key(corresp[i]):
					saliencyG[corresp[i]] = {}
				if not saliencyX.has_key(corresp[i]):
					saliencyX[corresp[i]] = {}
				#print self.model.inputTransform, self.model.middle
				#print self.model.middle[1].weight.grad.size(
				tmp = t.abs(t.sum(self.model.middle[1].weight.grad,0)).cpu()
				#print "g grad"
				#print tmp, t.max(tmp)
				tmp = np.array(tmp)
				#tmp1 = tmp
				#r = self.sortGenes(tmp, genes, 100)
				#tmp = t.abs(x.grad[i,:,:])
				saliencyG[corresp[i]][phen] = self.sortGenes(tmp, genes, 100)

				#print x.grad.size()
				tmp = t.abs(t.sum(x.grad[0,:,:], 1)).cpu()
				#print tmp.size()
				#print "x grad"
				#print tmp, t.max(tmp)
				#raw_input()
				#print tmp, tmp1
				saliencyX[corresp[i]][phen] = self.sortGenes(tmp, genes, 100)
				#saliency[corresp[i]].append(r)
				#saliency[corresp[i]].append(np.array(tmp.cpu(), dtype=np.float16))
				optimizer.zero_grad()
				assert t.sum(self.model.middle[1].weight.grad) == 0
				assert t.sum(x.grad) == 0
			i += 1
		return saliencyG, saliencyX

	def predictSALInput(self, X, Y, corresp, device, saliency, genes, IGNORE_INDEX = -9999):
		self.model.eval()
		print "Predicting SAL..."
		dataset = ATDataset(X, Y)
		loader = DataLoader(dataset, batch_size=len(X), shuffle=False, sampler=None, num_workers=0)
		assert len(X) == len(Y) == len(corresp)
		for sample in loader:#runs only once
			x, y = sample
			x = x.to(device)
			x.requires_grad_()
			y = y.to(device)
			yp = self.model.forward(x)
			#print yp.size()
			for phen in xrange(0, y.size()[1]):
				sys.stdout.write("\rphen %d/%d" % (phen, y.size()[1]))
				t.sum(yp[:,phen]).backward(retain_graph=True)
				i = 0
				while i < len(X):
					if not saliency.has_key(corresp[i]):
						saliency[corresp[i]] = []
					tmp = t.sum(t.abs(x.grad[i,:,:]), 1).cpu()
					r = self.sortGenes(tmp, genes, 100)
					#tmp = t.abs(x.grad[i,:,:])
					saliency[corresp[i]].append(r)
					#saliency[corresp[i]].append(np.array(tmp.cpu(), dtype=np.float16))
					i+=1
				x.grad.zero_()
				assert t.sum(x.grad) == 0
		return saliency

	def sortGenes(self, grad, genes, THR):
		grad = grad.tolist()
		assert len(grad) == len(genes)
		geneNames = genes.keys()
		i = 0
		r = []
		while i < len(grad):
			r.append((grad[i], geneNames[i]))
			i+=1
		r = sorted(r, key=lambda x:x[0], reverse=True)[:THR]
		tmpGrad = []
		tmpGene = []
		for e in r:
			tmpGrad.append(e[0])
			tmpGene.append(e[1])
		return (tmpGene, np.array(tmpGrad))
		#print r[:THR]
		#return r[:THR]
