import torch as t
from copy import  deepcopy as dc
import math

class LeakyReLU6(t.nn.Module):
	def __init__(self, slope = 0.01, min = -1, max = 1):
			super(LeakyReLU6, self).__init__()
			self = LeakyHardTanh(0.01, min=0, max = 6)


class LeakyHardTanh(t.nn.Module):
        
	def __init__(self, slope = 0.01, min = -1, max = 1):
			super(LeakyHardTanh, self).__init__()
			self.slope = slope
			self.min = min
			self.max = max
			
	def forward(self, a):
			centr = a.clamp(self.min, self.max)             
			over = a.gt(1).float()*self.slope
			below = a.lt(-1).float()*self.slope             
			return centr + a*over + a*below


class ResidualBlock(t.nn.Module):

	"""
	This class puts an identity connection around a module (which become the residual connection).
	"""

	def __init__(self, embModule):
		super(ResidualBlock, self).__init__()
		self.embModule = embModule

	def forward(self, x):
		#print x.size()
		o = self.embModule(x)
		#print "uscito",o.size()
		o = x + o
		#print o.size()
		return o

class InceptionSimple(t.nn.Module):

	"""
	This class implements a simple version of inception block.
	Takes in the number of input channels, the desired size of the output channels, the list of CNNkernel sizes.
	If pool is not None, it takes the pooling function that you want to add to the cnn layers.
	If shirnk is not none, it takes the final number of channels you want as output. An 1x1 CNN will be applied to shrink the channels into the number specified by shrink (useful if used as residual block).
	"""

	def __init__(self, in_channels, out_channelsList=[10,10,10], kernSizeList = [1,5,15], pool=None, activation=t.nn.LeakyReLU, conv=t.nn.Conv1d, shrink=None, dev="cuda"):
		super(InceptionSimple, self).__init__()
		self.modList = t.nn.ModuleList()
		assert len(kernSizeList) == len(out_channelsList)
		#print conv
		for i, ks in enumerate(kernSizeList):
			pad = max(0,int(math.floor(ks/2.0)))
			#print pad, ks, out_channelsList[i]
			self.modList.append(t.nn.Sequential(conv(in_channels, out_channels=out_channelsList[i], kernel_size=ks, padding=pad), activation()))
		self.shrinker = None
		if shrink != None:
			self.shrinker = conv(sum(out_channelsList), shrink, kernel_size=1, padding = 0) 
		#finalAct = dc(act)	

	def forward(self, x):
		tmp = []
		for m in self.modList:
			t1 = m(x)
			#print "inc", t1.size()
			tmp.append(t1)
		out = t.cat(tmp,1)
		#print "inc", out.size()
		if self.shrinker != None:
			out = self.shrinker(out)
		#print "inc1", out.size()
		return out
		#return self.finalAct(out)

class AUCLoss():
	
	def __init__(self):
		pass
		
	def __call__(self,pr,tr):		
		pr=pr.view(-1)
		tr=tr.view(-1)
		m=tr.ge(0.5)
		pos_media=t.mean(t.masked_select(pr,m))
		neg_media=t.mean(t.masked_select(pr,~m))
		return -(pos_media-neg_media)
