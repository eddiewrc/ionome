#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  readSaltData.py
#  
#  Copyright 2022 eddiewrc <eddiewrc@alnilam>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import numpy as np
import pickle

def readAraPathways(f):
	ifp = open(f, "r")
	lines = ifp.readlines()
	ifp.close()
	header = lines.pop(0)
	g2p = {}
	p2g = {}
	
	for l in lines:
		tmp = l.strip().split("\t")
		print(tmp)
		clust = tmp[0]
		desc = tmp[6]
		#if not "AT" in desc:
		#	continue
		print(clust, desc)
		#print
		#input()
		if not clust in p2g:
			p2g[clust] = set()
		p2g[clust].add(desc)
		if not desc in g2p:
			g2p[desc] = set()
		g2p[desc].add(clust)
	print("Found %d pathways and %d genes"% (len(p2g), len(g2p)))
	return g2p, p2g

def main(args):
	g2p, p2g = readAraPathways("dataset/ara_pathways.20210325.txt")
	genesList = pickle.load(open("ATgeneslist.cPickle","rb"))
	c = 0
	for g in genesList:
		if g in g2p:
			c+=1
	print(100*c/float(len(genesList)))

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
